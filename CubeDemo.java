/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cubedemo;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.stage.Stage;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration; 
import javafx.animation.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.CheckBox;
import javafx.scene.PointLight;
import javafx.scene.AmbientLight;
import javafx.scene.shape.DrawMode;
//import javafx.scene.image.Image;

/**
 *
 * @author Lawrence Makin
 */
public class CubeDemo extends Application {
    
    @Override
    public void start(Stage primaryStage) {

        // Slider to change X rotation of Box
        Slider sliderRotx = new Slider(-180.0d, 180.0d, 60.0d);
        setSliderAttribs(sliderRotx);
        
        Label labelRotx = new Label("X");
        Label labelRotxVal = new Label(
        Double.toString(sliderRotx.getValue()));
        HBox hboxRotx = new HBox(10, labelRotx, sliderRotx, labelRotxVal);
        hboxRotx.setPadding(new Insets(10, 10, 10, 10));
        
        sliderRotx.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    labelRotxVal.setText(String.format("%.2f", new_val));
            }
        });
        
        // Slider to change y rotation of Box
        Slider sliderRoty = new Slider(-180.0d, 180.0d, 60.0d);
        setSliderAttribs(sliderRoty);
        
        Label labelRoty = new Label("Y");
        Label labelRotyVal = new Label(
        Double.toString(sliderRoty.getValue()));
        HBox hboxRoty = new HBox(10, labelRoty, sliderRoty, labelRotyVal);
        hboxRoty.setPadding(new Insets(10, 10, 10, 10));
        
        sliderRoty.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    labelRotyVal.setText(String.format("%.2f", new_val));
            }
        });
        
        // Slider to change Z rotation of Box
        Slider sliderRotz = new Slider(-180.0d, 180.0d, 60.0d);
        setSliderAttribs(sliderRotz);
        
        Label labelRotz = new Label("Z");
        Label labelRotzVal = new Label(
        Double.toString(sliderRotz.getValue()));
        HBox hboxRotz = new HBox(10, labelRotz, sliderRotz, labelRotzVal);
        hboxRotz.setPadding(new Insets(10, 10, 10, 10));
        
        sliderRotz.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    labelRotzVal.setText(String.format("%.2f", new_val));
            }
        });
        
        // Vertical box pane to organise sliders
        VBox vboxRot = new VBox(10, new Label("Rotate cube around axes "
                + "(in degrees):"), hboxRotx, hboxRoty, hboxRotz);
        vboxRot.setPadding(new Insets(10, 10, 10, 10));
        
        // Create a new Box
        Box box = new Box(100, 100, 100);
        
        // Set initial Box rotations
        Rotate x = new Rotate();
        x.axisProperty().setValue(Rotate.X_AXIS);
        setBoxRotate(box, x, 45);
        
        Rotate y = new Rotate();
        y.axisProperty().setValue(Rotate.Y_AXIS);
        setBoxRotate(box, y, 45);

        Rotate z = new Rotate();
        z.axisProperty().setValue(Rotate.Z_AXIS);
        setBoxRotate(box, z, 0);
        
        // Bind slider values to axis rotation angles
        sliderRotx.valueProperty().bindBidirectional(x.angleProperty());
        sliderRoty.valueProperty().bindBidirectional(y.angleProperty());
        sliderRotz.valueProperty().bindBidirectional(z.angleProperty());
        
        // Set the initial Box material properties
        PhongMaterial material = new PhongMaterial();
        
        /* Image sourced from: https://bentrubewriter.com/2012/04/26/
           fractals-you-can-draw-the-hilbert-curve-or-what-the-labyrinth-
           really-looked-like/ */
        
        //material.setDiffuseMap(new Image("\\KochCurve.png"));
        material.setDiffuseColor(Color.PURPLE);
        material.setSpecularColor(Color.PURPLE);
        box.setMaterial(material);
        
        // Create a colour picker and set up its event handler
        ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(Color.PURPLE);
        
        colorPicker.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent t) {
                Color c = colorPicker.getValue();
                PhongMaterial material = new PhongMaterial();
                material.setDiffuseColor(c);
                material.setSpecularColor(c);
                box.setMaterial(material);
            }
        });
        
        // Rotation animation settings
        RotateTransition rotateTransition = new RotateTransition(); 
        rotateTransition.setDuration(Duration.millis(5000)); 
        rotateTransition.setNode(box);       
        rotateTransition.setByAngle(360); 
        rotateTransition.setCycleCount(10); 
        rotateTransition.setAutoReverse(true); 
        
        // Create buttons to animate Box, set event handlers
        Button btnAnimate = new Button();
        btnAnimate.setText("Animate Cube");
        Button btnAniStop = new Button();
        btnAniStop.setText("Stop Cube");
        
        btnAnimate.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                rotateTransition.play();
            }
        });
        
        btnAniStop.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                rotateTransition.stop();
            }
        });
        
        // Checkbox for point light
        CheckBox drawMode = new CheckBox("Wireframe");
        drawMode.selectedProperty().addListener(observable -> {
            if (drawMode.isSelected()) {
                box.setDrawMode(DrawMode.LINE);
            } else {
                box.setDrawMode(DrawMode.FILL);
            }
        });
        
        VBox hboxBox = new VBox(50, drawMode, box);
        hboxBox.setPadding(new Insets(10, 10, 10, 10));
        hboxBox.setAlignment(Pos.CENTER);
        
        // Set up an ambient light source
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(Color.WHITE);
        ambient.setTranslateX(100);
        ambient.setTranslateY(-100);
        ambient.setTranslateZ(-300);
        
        // Checkbox for ambient light
        CheckBox aLight = new CheckBox("Ambient Light");
        aLight.selectedProperty().addListener(observable -> {
            if (aLight.isSelected()) {
                hboxBox.getChildren().add(ambient);
            } else {
                hboxBox.getChildren().remove(ambient);
            }
        });
        
        // Set up a point light source
        PointLight point = new PointLight();
        point.setColor(Color.WHITE);
        point.setTranslateX(100);
        point.setTranslateY(100);
        point.setTranslateZ(-300);
        
        // Checkbox for point light
        CheckBox pLight = new CheckBox("Point Light");
        pLight.selectedProperty().addListener(observable -> {
            if (pLight.isSelected()) {
                hboxBox.getChildren().add(point);
            } else {
                hboxBox.getChildren().remove(point);
            }
        });
        
        // Horizontal box to organise the colour picker and light sources
        HBox hboxColorLight = new HBox(10, colorPicker, aLight, pLight);
        hboxColorLight.setPadding(new Insets(10, 10, 10, 10));
        
        // Horizontal box to organise the animation buttons
        HBox hboxAni = new HBox(10, new Label("Animate the Cube"), btnAnimate, 
                btnAniStop);
        hboxAni.setPadding(new Insets(10, 10, 10, 10));
        
        // Vertical box to organise previous two horizontal boxes vertically
        VBox vboxBottom = new VBox(10, hboxColorLight, hboxAni);
        vboxBottom.setPadding(new Insets(10, 10, 10, 10));
        vboxBottom.setAlignment(Pos.CENTER);
        
        // Create a BorderPane and organise hbox/vbox panes by zone
        BorderPane root = new BorderPane();

        root.setTop(vboxRot);
        root.setCenter(hboxBox);
        root.setBottom(vboxBottom);
        
        // Set the scene
        Scene scene = new Scene(root, 400, 650);
        
        primaryStage.setTitle("3D Cube Tech Demo");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    // Set slider attributes
    private void setSliderAttribs(Slider slider) {
        slider.setMinWidth(275.0d);
        slider.setMaxWidth(400.0d);
        slider.setMajorTickUnit(10.0d);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
    }
    
    // Set box rotation attributes
    private void setBoxRotate(Box box, Rotate rotate, int angle) {
        rotate.setAngle(angle);
        box.getTransforms().add(rotate);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
