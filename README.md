JavaFX Demo

----------------------------------------------------------------------------------------------------

Description:

A demo to showcase a few features of JavaFX, such as drawing an animatable cube, changing colours,
rotational sliders etc.

----------------------------------------------------------------------------------------------------

Pre-requisites:

JDK 1.8 to build and run (later versions no longer support JavaFX)

----------------------------------------------------------------------------------------------------

Compile options:

Due to JavaFX libaries used, source code is best compiled in an IDE (netbeans was used)

----------------------------------------------------------------------------------------------------

Execution:

Run from IDE or export to .jar file